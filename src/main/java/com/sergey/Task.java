package com.sergey;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

public class Task implements Runnable {

    private String fileName;

    public Task(String fileName) {
        this.fileName = fileName;
        System.out.println("Create new Task " + fileName);
    }

    @Override
    public void run() {
        try(FileWriter fileWriter = new FileWriter(fileName);
            PrintWriter printWriter = new PrintWriter(fileWriter)) {

            Thread currentThread = Thread.currentThread();
            while (!currentThread.isInterrupted() &&
                    DataGenerator.getInstance().counter.get() < Main.RECORD_COUNT.get()) {
                synchronized (DataGenerator.class) {
                    Record record = DataGenerator.getInstance().generateRecord();
                    printWriter.println(record.toString());
                }
                TimeUnit.MILLISECONDS.sleep(50);
            }
        }
        catch (Exception ex) {
            System.out.println(ex);
        }
        finally {
            System.out.println("End Thread");
        }
    }
}
