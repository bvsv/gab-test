package com.sergey;

import com.sergey.domain.Office;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

public final class DataGenerator {

    private static final double OPERATION_SUM_MAX = 100_000.50;
    private static final double OPERATION_SUM_MIN = 10_000.12;

    private List<Office> officeList = new ArrayList<>();
    public AtomicLong counter = new AtomicLong(0);
    
    private DataGenerator() {}

    public static DataGenerator getInstance() {
        return DataGeneratorHolder.instance;
    }

    public static LocalDateTime generateDate() {

        LocalDateTime n = LocalDateTime.now();
        LocalDateTime endDate = n.truncatedTo(ChronoUnit.DAYS).with(TemporalAdjusters.firstDayOfYear());

        LocalDateTime localDateTime = n.plusYears(-1);
        LocalDateTime startDate = localDateTime.truncatedTo(ChronoUnit.DAYS).with(TemporalAdjusters.firstDayOfYear());

        Instant instantStart = startDate.toInstant(ZoneOffset.UTC);
        Instant instantEnd = endDate.toInstant(ZoneOffset.UTC);

        long random = ThreadLocalRandom
                .current()
                .nextLong(instantStart.getEpochSecond(), instantEnd.getEpochSecond());
        Instant instant = Instant.ofEpochSecond(random);

        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    public static BigDecimal generateOperationSum() {
        double random = ThreadLocalRandom.current().nextDouble(OPERATION_SUM_MIN, OPERATION_SUM_MAX);

        return new BigDecimal(random, MathContext.DECIMAL32);
    }

    public Record generateRecord() {
        Record record = new Record(generateDate(),
                counter.incrementAndGet(),
                generateOperationSum(),
                officeList.get(new Random().nextInt(officeList.size())).toString());
        return record;
    }

    public void setOfficeList(List<Office> officeList) {
        this.officeList = officeList;
    }

    private static class DataGeneratorHolder {
        private DataGeneratorHolder() {}
        private static final DataGenerator instance = new DataGenerator();
    }
}
