package com.sergey.domain;

import java.util.Objects;

public final class Office {

    private final String officeName;

    public Office(String officeName) {
        this.officeName = officeName;
    }

    public String getOfficeName() {
        return officeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Office office = (Office) o;
        return Objects.equals(officeName, office.officeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(officeName);
    }

    @Override
    public String toString() {
        return officeName;
    }
}
