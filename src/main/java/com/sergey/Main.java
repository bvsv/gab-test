package com.sergey;

import com.sergey.domain.Office;
import com.sergey.exceptions.OfficeException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class Main {

    public static AtomicLong RECORD_COUNT;

    public static void main(String[] args) {
        if(Objects.isNull(args)) {
            System.out.println("Для работы программы укажите входные параметры!");
            return;
        }
        if(args.length < 3) {
            System.out.println("Введено недостаточное количество параметров для работы приложения!");
            return;
        }

        String officeSourceFileName = args[0];

        try{
            RECORD_COUNT = new AtomicLong(Long.parseLong(args[1]));
        } catch (NumberFormatException nfEx) {
            System.out.println("В качестве второго параметра должно быть число!");
            return;
        }

        List<String> fileNames = Arrays.asList(args).subList(2, args.length);
        List<Office> officeList = new ArrayList<>();

        try{
            officeList = OfficeReader.getOfficeList(officeSourceFileName);
        } catch (OfficeException e) {
            System.out.println("В ходе выполнения программы возникла ошибка: " + e.getMessage());
            return;
        }

        DataGenerator.getInstance().setOfficeList(officeList);

        ExecutorService executorService = Executors.newFixedThreadPool(fileNames.size());
        fileNames.forEach(p -> {
            Runnable r = new Task(p);
            executorService.submit(r);
        });


        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }

    }

}
