package com.sergey;

import com.sergey.domain.Office;
import com.sergey.exceptions.OfficeException;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class OfficeReader {

    /**
     * Получение списка Точек продаж из тесктового файла
     * @param fileName Имя файла, в котором хранятся точки продаж
     * @return Список точек продаж
     * @throws OfficeException В случае, если файл не был найден или не удалось прочитать из файла выбросится исключение
     */
    public static List<Office> getOfficeList(String fileName) throws OfficeException {
        if (Objects.isNull(fileName))
            throw new OfficeException("Имя файла с точками продаж не может быть null");

        List<Office> result = new ArrayList<>();

        try (Stream<String> stream = Files.lines( Paths.get(fileName), StandardCharsets.UTF_8)) {
            stream.forEach(p -> result.add(new Office(p)));
            return result;
        }
        catch (NoSuchFileException noSuchFile) {
            throw new OfficeException("Не найден файл " + fileName);
        }
        catch (IOException ex) {
            throw new OfficeException(ex.toString());
        }
    }
}
