package com.sergey;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class OfficeGenerator {

    public static void generateOffice(String fileName) {
        int n = 7;

        try(FileWriter fileWriter = new FileWriter(fileName);
            PrintWriter printWriter = new PrintWriter(fileWriter)) {
            for(int i = 0; i < n; i++) {
                printWriter.println("Office" + i);
            }
        }
        catch (IOException ioEx) {
            System.out.println(ioEx);
        }
    }
}
