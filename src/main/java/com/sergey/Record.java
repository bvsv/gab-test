package com.sergey;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Record {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private LocalDateTime recordDate;
    private long recordNumber;
    private BigDecimal paymentSum;
    private String officeName;

    public Record(LocalDateTime recordDate, long recordNumber, BigDecimal paymentSum, String officeName) {
        this.recordDate = recordDate;
        this.recordNumber = recordNumber;
        this.paymentSum = paymentSum;
        this.officeName = officeName;
    }

    @Override
    public String toString() {
        return  recordDate.format(FORMATTER) + ";" + recordNumber + ";" + paymentSum + ";" + officeName;
    }
}
