package com.sergey.exceptions;

public class OfficeException extends Exception {

    public OfficeException(String message) {
        super(message);
    }
}
